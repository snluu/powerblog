@echo off

echo Building files...
Powershell -ExecutionPolicy RemoteSigned -File "%DEPLOYMENT_SOURCE%\build.ps1"

echo Copying files...
xcopy "%DEPLOYMENT_SOURCE%\output" "%DEPLOYMENT_TARGET%" /Y /S /E