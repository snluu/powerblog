if (-not $PBROOT) {
    Set-Variable -Name PBROOT -Option ReadOnly -Value (Split-Path -Parent -Path $MyInvocation.MyCommand.Definition)    
}

$pbEnv = ${env:PBENV}

Write-Output '==='
Write-Output 'Installing NuGet packages'
& "$PBROOT\bin\nuget" install "$PBROOT\bin\packages.config" -OutputDirectory "$PBROOT\bin\packages"


# include other script files
Write-Output ''
Write-Output 'Pulling in script files'
. "$PBROOT\scripts\post.ps1"
. "$PBROOT\scripts\config.ps1"
. "$PBROOT\scripts\files.ps1"


Write-Output '==='
Write-Output 'Building blog posts'

$config = GetConfig -configFilePath "$PBROOT\data\config.json"
$theme = $config.Theme
$themeFolder = "$PBROOT\data\themes\$theme"
$postsFolder = "$PBROOT\data\posts"
if ($pbEnv -eq 'DEV') {
    $postsFolder = "$PBROOT\data\devposts"
}

ProcessAllPosts -postsFolder $postsFolder -outputDir "$PBROOT\output" -themeFolder $themeFolder -config $config

Write-Output '==='
Write-Output 'Copying theme folder'
CopyTheme -outputDir "$PBROOT\output" -themeFolder $themeFolder