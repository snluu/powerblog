Write-Output('post.ps1')

Add-Type -Path "$PBROOT\bin\packages\MarkdownSharp.1.13.0.0\lib\35\MarkdownSharp.dll"
Add-Type -Path "$PBROOT\bin\packages\Nustache.1.13.8.22\lib\net20\Nustache.Core.dll"

function SetHeaderData() {
    param ($post, [string]$raw)

    if ([string]::IsNullOrWhiteSpace($raw) -or ($raw.Length -lt 3)) {
        return
    }

    $colonIndex = $raw.IndexOf(':')
    if ($colonIndex -eq -1) { # colon not found
        return
    }

    $dataType = $raw.Substring(0, $colonIndex).Trim().ToLowerInvariant()
    $dataValue = $raw.Substring($colonIndex + 1).Trim()

    switch ($dataType) {
        'title' { $post.Title = $dataValue }
        'slug' { $post.Slug = $dataValue.ToLowerInvariant() }
        'date' { $post.Date = [DateTime]::Parse($dataValue) }
        'noindex' { $post.NoIndex = ($dataValue.Trim().ToLowerInvariant() -eq 'true') }
        'nocomment' { $post.NoComment = ($dataValue.Trim().ToLowerInvariant() -eq 'true') }
    }
}


function PreparePath() {
    param ([string]$outputDir, [string]$slug)

    if (-not (Test-Path -Path $outputDir -PathType Container)) {
        New-Item -ItemType directory -Path $outputDir > $null
    }

    $filePath = Join-Path -ChildPath $slug -Path $outputDir
    if (-not (Test-Path -Path $filePath -PathType Container)) {
        # create folder with slug
        New-Item -ItemType directory -Path $filePath > $null
    }

    # create empty index file
    $filePath = Join-Path -ChildPath 'index.html' -Path $filePath
    if (Test-Path -Path $filePath) {
        Remove-Item -Path $filePath
    }

    New-Item -ItemType file -Path $filePath > $null

    return $filePath
}


function GetLinks() {
    param($config, [string]$root)
    
    $links = @()
    
    for ($i = 0; $i -lt $config.Links.Count; $i++) {
        $l = $config.Links[$i]
        
        $url = $l.Link
        if ($url.StartsWith('/')) {
            $url = $root + $url
        }
        
        $links += (@{
            Title = $l.Title
            Link = $url
        })
    }
    
    return $links
}


function MarkdownToHtml() {
    param ([string]$markdownText)

    $markdownOption = New-Object -TypeName MarkdownSharp.MarkdownOptions
    $markdownOption.AutoHyperlink = $true

    $markdown = New-Object -TypeName MarkdownSharp.Markdown -ArgumentList $markdownOption
    
    return $markdown.Transform($markdownText)
}


function RenderPost() {
    param ($post, $config, [string]$outputDir, [string]$themeFolder)
    
    $htmlContent = (MarkdownToHtml -markdown $post.Content)
        
    $filePath = PreparePath -outputDir $outputDir -slug $post.Slug
    $templateFilePath = Join-Path -ChildPath 'post.html' -Path $themeFolder
    
    $templateData = @{
        PostTitle = $post.Title;
        PostDate = $post.DisplayDate;
        PostContent = $htmlContent;
        BlogTitle = $config.BlogTitle;
        BlogDescription = $config.BlogDescription;
        Links = GetLinks -config $config -root '../..';
        GoogleAnalyticsID = $config.GoogleAnalyticsID;
        DisqusID = $config.DisqusID;
        AllowComment = (-not $post.NoComment);
    }

    [Nustache.Core.Render]::FileToFile($templateFilePath, $templateData, $filePath, $null)
}


function RenderIndex() {
    param($allPosts, [int]$beginAt, [int]$endAt, [int]$pageNumber, [string]$outputDir, [string]$themeFolder, $config)
    
    $fileName = 'index.html'
    if ($pageNumber -gt 1) {
        $fileName = 'index-{0}.html' -f $pageNumber
    }
    $templateFilePath = Join-Path -ChildPath 'index.html' -Path $themeFolder
    $filePath = Join-Path -ChildPath $fileName -Path $outputDir

    $posts = @()

    for ($i = $beginAt; $i -le $endAt; $i++) {
        $post = $allPosts[$i]
        $posts += (@{
            Title = $post.Title;
            Date = $post.DisplayDate;
            Preview = (MarkdownToHtml -markdownText $post.Preview);
            Slug = $post.Slug
        })
    }

    $nextPage = ('index-{0}.html' -f ($pageNumber + 1))
    $prevPage = ('index-{0}.html' -f ($pageNumber - 1))
    if ($pageNumber -eq 2) {
        $prevPage = 'index.html'
    }

    $templateData = @{
        HasNextPage = ($endAt -ne ($allPosts.Count - 1));
        HasPrevPage = ($pageNumber -gt 1);
        PrevPage = $prevPage;
        NextPage = $nextPage;
        BlogTitle = $config.BlogTitle;
        BlogDescription = $config.BlogDescription;
        Posts = $posts;
        Links = GetLinks -config $config -root '.';
        GoogleAnalyticsID = $config.GoogleAnalyticsID;
    }

    [Nustache.Core.Render]::FileToFile($templateFilePath, $templateData, $filePath, $null)
}


function ReadPost() {
    param ([string]$filePath)

    $post = New-Object PSObject -Property @{
        Title = '';
        Date = [DateTime]::Now;
        Slug = '';
        Preview = '';
        Content = '';
        DisplayDate = '';
        NoIndex = $false;
        NoComment = $false;
    }
    $previewSb = New-Object -TypeName System.Text.StringBuilder
    $contentSb = New-Object -TypeName System.Text.StringBuilder

    $reader = [System.IO.File]::OpenText($filePath)
    $blankLineCount = 0

    try {
        for (;;) {
            $line = $reader.ReadLine()
            if ($line -eq $null) {
                break
            }

            if ([string]::IsNullOrWhiteSpace($line)) {
                $blankLineCount = $blankLineCount + 1
            }
                        
            if ($blankLineCount -eq 0) {
                # metadata
                SetHeaderData -post $post -raw $line
            } else {
                if ($blankLineCount -eq 1) {
                    # preview
                    $previewSb.AppendLine($line) > $null
                }

                $contentSb.AppendLine($line) > $null
            }
        }
    } finally {
        $reader.Close()
    }

    $post.Preview = $previewSb.ToString()
    $post.Content = $contentSb.ToString()
    $post.DisplayDate = $post.Date.ToString('yyyy-MM-dd')

    if ([string]::IsNullOrWhiteSpace($post.Slug)) {
        $post.Slug = (Get-ChildItem $filePath).BaseName
    }

    return $post
}


function ProcessPost() {
    param ([string]$filePath, [string]$outputDir, [string]$themeFolder, $config)

    $post = ReadPost -filePath $filePath
    RenderPost -post $post -outputDir $outputDir -themeFolder $themeFolder -config $config

    return $post
}


function ProcessAllPosts() {
    param ([string]$postsFolder, [string]$outputDir, [string]$themeFolder, $config)

    $postsOutputDir = Join-Path -ChildPath "posts" -Path $outputDir
    $allPosts = @{}

    Get-ChildItem "$postsFolder\*" -Include @('*.md', '*.markdown') |
    foreach {
        Write-Output $_.Name
        $p = ProcessPost -filePath $_.FullName -outputDir $postsOutputDir -themeFolder $themeFolder -config $config

        if ($allPosts.ContainsKey($p.Slug)) {
            throw ('Duplicate slug {0}' -f $p.Slug)
        }

        if (-not $p.NoIndex) {
            $allPosts.Add($p.Slug, $p)
        }
    }

    if ($allPosts.Count -eq 0) {
        return
    }

    Write-Output '==='
    Write-Output 'Building index pages'

    # order posts by date
    $allPosts = $allPosts.Values

    if ($allPosts.Count -gt 1) {
        $allPosts = $allPosts | Sort-Object -Property Date -Descending
    }
    
    $postsPerPage = $config.PostsPerPage
    $pageNumber = 0
    
    for ($i = 0; $i -lt $allPosts.Count; $i += $postsPerPage) {
        $pageNumber++
        $endAt = $i + $postsPerPage - 1
        if ($endAt -gt ($allPosts.Count - 1)) {
            $endAt = $allPosts.Count - 1
        }

        Write-Output ('Index page {0}' -f $pageNumber)

        RenderIndex -allPosts $allPosts -beginAt $i -endAt $endAt -pageNumber $pageNumber -outputDir $outputDir -themeFolder $themeFolder -config $config
    }
}