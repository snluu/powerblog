Write-Output 'config.ps1'

function GetConfig($configFilePath) {    
    return (Get-Content -Raw -Path $configFilePath | ConvertFrom-Json)
}