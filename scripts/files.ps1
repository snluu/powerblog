Write-Output('files.ps1')

function CopyTheme() {
    param([string]$outputDir, [string]$themeFolder)
    
    $outputThemeDir = Join-Path -ChildPath 'theme' -Path $outputDir
    if (Test-Path -Path $outputThemeDir -PathType Container) {
        Remove-Item -Path $outputThemeDir -Force -Recurse
    }
    
    New-Item -ItemType directory -Path $outputThemeDir > $null
    
    & "xcopy" """$themeFolder""" """$outputThemeDir""" "/Y" "/S" "/E"
}